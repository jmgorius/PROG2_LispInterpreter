.SUFFIXES:
.SUFFIXES: .l .y .cpp .hpp .o .c .h .orig .depend
.PHONY: clean all syntax astyle enscript softclean zip doc

SRC_DIR := src/
INCLUDE_DIR := include/
LEXPARSE_DIR := lex_parse/

TESTS_DIR := tests/

CPP_FILES := $(wildcard $(SRC_DIR)*.cpp)
HPP_FILES := $(wildcard $(INCLUDE_DIR)*.hpp)
O_FILES := $(CPP_FILES:%.cpp=%.o)

TESTS_CPP_FILES := $(wildcard $(TESTS_DIR)*.cpp)
TESTS_O_FILES := $(TESTS_CPP_FILES:%.cpp=%.o)

CC_FLAGS := -Wall -Wextra
CC_FLAGS += -Wno-unused-function -Wno-unused-variable -Wno-unused-parameter
CC_FLAGS += -O0 -std=c++14
# Debug
CC_FLAGS += -g
CC_FLAGS += -Iinclude

INTERPRETER := lisp
TESTS := $(TESTS_CPP_FILES:%.cpp=%.out)

default: all
all: build_tests build
build: $(INTERPRETER)
build_tests: $(TESTS)
run: build
	rlwrap ./$(INTERPRETER)
test: build_tests
	bash tests.sh

DEPEND_FILE := lisp.depend
include $(DEPEND_FILE)

$(O_FILES): %.o: %.cpp Makefile $(HPP_FILES)
	g++ $(CC_FLAGS) $< -c -o $@

$(TESTS_O_FILES): %.o: %.cpp Makefile $(HPP_FILES)
	g++ $(CC_FLAGS) $< -c -o $@

## Syntactic analyser #########################

C_SYNTAX_FILES	:= $(LEXPARSE_DIR)lisp.yy.c $(LEXPARSE_DIR)lisp.tab.c
O_SYNTAX_FILES	:= $(C_SYNTAX_FILES:%.c=%.o)

syntax: $(O_SYNTAX_FILES)

$(LEXPARSE_DIR)lisp.yy.c: $(LEXPARSE_DIR)lisp.lex $(LEXPARSE_DIR)lisp.tab.h Makefile
	flex -o $@ $<

## lisp.tab.h is generated as a side-effect of lisp.tab.c
## First, we construct the name of the C file
## Then we call a common construction rule with the right argument
$(LEXPARSE_DIR)lisp.tab.c: SOURCE_C_FILE=$@
$(LEXPARSE_DIR)lisp.tab.h: SOURCE_C_FILE=$(@:%.h=%.c)
$(LEXPARSE_DIR)lisp.tab.c $(LEXPARSE_DIR)lisp.tab.h: $(LEXPARSE_DIR)lisp.yacc Makefile
	bison -d -o $(SOURCE_C_FILE) $<

read.o: $(LEXPARSE_DIR)lisp.tab.h

SYNTAX_FLAGS := -Wno-unused-function -Wno-unused-variable -Wno-sign-compare

$(O_SYNTAX_FILES): %.o: %.c
	g++ $(CC_FLAGS) $(SYNTAX_FLAGS) $< -c -o $@

###########################

$(INTERPRETER): $(O_FILES) $(O_SYNTAX_FILES) Makefile
	g++ $(CC_FLAGS) $(O_FILES) $(O_SYNTAX_FILES) -o $@

# $(TESTS): $(O_FILES) $(O_SYNTAX_FILES) $(TESTS_O_FILES) Makefile
# 	echo $^
# 	echo -------------------------
# 	g++ $(CC_FLAGS) $(TESTS_DIR)$@.o $(O_FILES) $(O_SYNTAX_FILES) -o $@

$(TESTS): %.out: %.cpp $(O_FILES) $(O_SYNTAX_FILES) $(TESTS_O_FILES) Makefile
	g++ $(CC_FLAGS) $(addsuffix .o, $(basename $@)) $(filter-out src/main.o, $(O_FILES)) $(O_SYNTAX_FILES) -o $@

ASTYLE_OPTIONS := --style=attach --indent=spaces=2

astyle:
	astyle $(ASTYLE_OPTIONS) $(CPP_FILES) $(HPP_FILES) $(TESTS_CPP_FILES)

softclean:
	-rm -f $(O_FILES) $(TESTS_O_FILES) $(O_SYNTAX_FILES) $(C_SYNTAX_FILES) $(LEXPARSE_DIR)lisp.tab.h
	-rm -f $(DEPEND_FILE)
	-rm -rf *.dSYM
	-rm -f *.orig

clean: softclean
	-rm -rf $(INTERPRETER) $(TESTS) listing.pdf
	-rm -rf doc/html

ENSCRIPT_BASE_FILES := cell object read main

ENSCRIPT_FILES := \
	$(shell \
	for F in $(ENSCRIPT_BASE_FILES); \
		do \
		if test -r $$F.hpp; then LIST+=" $$F.hpp"; fi; \
		if test -r $$F.cpp; then LIST+=" $$F.cpp"; fi; \
	done; \
	LIST+=" Makefile $(LEXPARSE_DIR)lisp.lex $(LEXPARSE_DIR)lisp.yacc"; \
	echo $$LIST)

ENSCRIPT_OPTIONS := --language=PostScript --missing-characters \
	--borders --nup=2 --word-wrap --mark-wrapped=arrow

enscript: astyle
	enscript $(ENSCRIPT_OPTIONS) $(ENSCRIPT_FILES) -o listing.ps
	pstopdf listing.ps listing.pdf; rm listing.ps

include $(DEPEND_FILE)

$(DEPEND_FILE): $(CPP_FILES) $(HPP_FILES) $(TESTS_CPP_FILES)
	@echo "Computing dependencies..."
	@# Trick to let a fake file with this name exist for the g++ -MM command
	@-touch $(LEXPARSE_DIR)lisp.tab.h
	g++ -MM $(CC_FLAGS) $(CPP_FILES) $(HPP_FILES) $(TESTS_CPP_FILES) > $@
	# g++ -MM $(CC_FLAGS) $(CPP_FILES) $(HPP_FILES) > $@
	@# Delete this fake file
	@-rm $(LEXPARSE_DIR)lisp.tab.h

doc:
	-doxygen doc/Doxyfile

zip: clean astyle
	zip -r ../Attias_Gorius_Le-Dilavrec_Taillandier.zip Makefile *.sh init.lsp include/*.hpp src/*.cpp lex_parse/*.lex lex_parse/*.yacc tests/*.?pp
