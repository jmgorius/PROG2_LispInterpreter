/**
 * \file fsubr.hpp
 * \brief This file contains the definitions of the fsubr's
 * \author Vidal Attias, Jean-Michel Gorius, Quentin Le Dilavrec & Valentin Taillandier
 * \version 1.0
 * \date February the 26th 2018
 *
 * fsubr's are subr's which systematically evaluate their arguments
 *
 */

#pragma once

#include "object.hpp"
#include "environment.hpp"

/// \namespace fsubr
/// This namespace contains all the fsubr symbols
namespace fsubr {

///\fn inline const Object& lambda()
///The lambda fsubr 'lambda' which is an implementation of the lamda calculus
inline const Object& lambda()
{
    static const Object o = Object::fromSymbol("lambda");
    return o;
}
///\fn inline const Object& quote()
///The quote fsubr 'quote' which bypasses the evaluation
inline const Object& quote()
{
    static const Object o = Object::fromSymbol("quote");
    return o;
}
///\fn inline const Object& setq()
///The setq fsubr 'setq' which stores a value in the Environment
inline const Object& setq()
{
    static const Object o = Object::fromSymbol("setq");
    return o;
}
inline const Object& define() {
  static const Object o = Object::fromSymbol("define");
  return o;
}
///\fn inline const Object& defun()
///The defun fsubr 'defun' which defines a function and turns it into lambda calculus
inline const Object& defun()
{
    static const Object o = Object::fromSymbol("defun");
    return o;
}
///\fn inline const Object& _if()
///The setq fsubr 'setq' which is the standard if structure
inline const Object& _if()
{
    static const Object o = Object::fromSymbol("if");
    return o;
}
///\fn inline const Object& cond()
///The cond fsubr 'cond' which is a conditional structure, it traverses the list and evals the first verified condition
inline const Object& cond()
{
    static const Object o = Object::fromSymbol("cond");
    return o;
}
///\fn inline const Object& progn()
///The progn fsubr 'progn' which evaluates a series of instructions and returns the last one
inline const Object& progn()
{
    static const Object o = Object::fromSymbol("progn");
    return o;
}
///\fn inline const Object& printenv()
///The printenv fsubr 'printenv' which outputs the current Environment
inline const Object& printenv()
{
    static const Object o = Object::fromSymbol("printenv");
    return o;
}
inline const Object& load() {
  static const Object o = Object::fromSymbol("load");
  return o;
}
inline const Object& let() {
  static const Object o = Object::fromSymbol("let");
  return o;
}

} // namespace fsubr


namespace fsubr {
namespace priv {
Object do_lambda(const Object& obj, const Object &lvals,Environment& env);
Object do_let(const Object& obj, Environment& env);
Object do_quote(const Object& obj, const Environment& env);
Object do_if(const Object& obj, Environment& env);
Object do_cond(const Object& obj, Environment& env);
Object do_progn(const Object& obj, Environment& env);
Object do_printenv(const Object& obj, const Environment& env);
}
}
