/**
 * \file fsubr.hpp
 * \brief The interpreter's environment
 * \author Vidal Attias, Jean-Michel Gorius, Quentin Le Dilavrec & Valentin Taillandier
 * \version 1.0
 * \date February the 26th 2018
 *
 * This file contains the definition of the Environment methods used to store variables
 *
 */

#pragma once

#include <utility>
#include <string>
#include <vector>

#include "object.hpp"
#include "exceptions.hpp"

///\class Environment environment.hpp
///This is the Environment class which is used to store variables
class Environment {
public:
  using binding = std::pair<std::string, Object>;
  int max_print_level = 4;

public:
  ///\fn Environment() = default
  /// Default constructor
  Environment() = default;
  ///\fn EEnvironment(const Environment &) = default
  /// Constructs an Environment from another Environment
  Environment(const Environment &) = default;
  ///\fn Environment &operator=(const Environment &) = default
  ///Assignment operator for Environment
  Environment &operator=(const Environment &) = default;
  ~Environment() = default;

<<<<<<< HEAD
	///\rn void add_binding(const Object& symbol, const Object& value)
	///Adds a binding in the Environment with a identifying symbol and a value
    void add_binding(const Object& symbol, const Object& value);
	///\fn const Object& get_value(const Object& symbol) const
	///Returns the value of a symbol stored in the Environment
    const Object& get_value(const Object& symbol) const;
	///\fn const Object& get_value(const Object& symbol) const
	///Removes the last inserted binding
	inline void pop_binding() { bindings.pop_back(); }
	///\fn inline size_t size() const { return bindings.size(); }
	/// Returns the size of a binding
	inline size_t size() const { return bindings.size(); }
	///\fn void add_largs(const Object& lpars, const Object& lvals)
	/// Adds a certain amount of values
	void add_largs(const Object& lpars, const Object& lvals);
	///\fn inline void clear() { bindings.clear(); }
	///Clears the Environment
	inline void clear() { bindings.clear(); }
	///\fn void print() const
	///Prints the Environment
	void print() const;
=======
  ///\rn void add_binding(const Object& symbol, const Object& value)
  ///Adds a binding in the Environment with a identifying symbol and a value
  void add_binding(const Object& symbol, const Object& value);
  ///\fn const Object& get_value(const Object& symbol) const
  ///Returns the value of a symbol stored in the Environment
  const Object& get_value(const Object& symbol) const;
  ///\fn const Object& get_value(const Object& symbol) const
  ///Removes the last inserted binding
  inline void pop_binding() {
    bindings.pop_back();
  }
  ///\fn inline size_t size() const { return bindings.size(); }
  /// Returns the size of a binding
  inline size_t size() const {
    return bindings.size();
  }
  ///\fn void add_largs(const Object& lpars, const Object& lvals)
  /// Adds a certain amount of values
  void add_largs(const Object& lpars, const Object& lvals);
  ///\fn inline void clear() { bindings.clear(); }
  ///Clears the Environment
  inline void clear() {
    bindings.clear();
  }

  void print() const;
>>>>>>> bc2e855131a5a0fc25920411dbf8b56ae2d4e590

private:
	///\fn void print_level(int n) const
	/// Displays the environment up to the n-th depth
	void print_level(int n) const;

private:
  std::vector<binding> bindings;
};

extern Environment global_env;
