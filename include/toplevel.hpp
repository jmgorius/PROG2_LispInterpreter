/**
 * \file toplevel.hpp
 * \brief Managing the Lisp's toplevel
 * \author Vidal Attias, Jean-Michel Gorius, Quentin Le Dilavrec & Valentin Taillandier
 * \version 1.0
 * \date February the 26th 2018
 *
 * This file manages the Lisps's toplevel, the interface between the Lisp core and the user.
 *
 */


#pragma once

#include "object.hpp"

/// \fn void init()
/// Initializes the toplevel with the buil-in subr's
void init();

/// \fn void restart()
/// Resets the toplevel and applies init()
void restart();

/// \fn void handle_directive(const Object&)
/// Handles an object from the parser's output and acts adequately
bool handle_directive(const Object&);
