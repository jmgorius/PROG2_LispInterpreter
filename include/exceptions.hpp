/**
 * \file exceptions.hpp
 * \brief All the exceptions used
 * \author Vidal Attias, Jean-Michel Gorius, Quentin Le Dilavrec & Valentin Taillandier
 * \version 1.0
 * \date February the 26th 2018
 *
 * This file contains the definition of all the exceptions used in the interpreter
 *
 */

#pragma once

#include <stdexcept>
#include <sstream>
#include <string>
#pragma once

#include "object.hpp"

///\class InterpreterException exceptions.hpp
///Base class of the other exception classes
class InterpreterException : public std::runtime_error {
public:
  InterpreterException(const std::string& msg)
    : std::runtime_error(msg) {}
};

///\class LispError exceptions.hpp
///Base class of the ApplyError and UnknownSymbolError classes
class LispError : public InterpreterException {
public:
  explicit LispError(const std::string& msg) : InterpreterException(msg) {}
  LispError(const std::string& msg, const Object& obj)
    : InterpreterException(msg + ": " + errorStringFromObject(obj)) {}

private:
  inline std::string errorStringFromObject(const Object& obj) {
    std::ostringstream ss;
    ss << obj;
    return ss.str();
  }
};

/// \class ApplyError exceptions.hpp
/// An exception which is called when and error is detected while applying
class ApplyError : public LispError {
public:
  ApplyError(const Object& obj) : LispError("Apply error", obj) {}
};

/// \class UnknownSymbolError exceptions.hpp
/// An exception which is called when and error an unknown symbol is used
class UnknownSymbolError : public LispError {
public:
  UnknownSymbolError(const Object& obj) : LispError("Unknown symbol", obj) {}
};
