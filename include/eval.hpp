/**
 * \file eval.hpp
 * \brief The file which manages the evaluation of Lisp expressions
 * \author Vidal Attias, Jean-Michel Gorius, Quentin Le Dilavrec & Valentin Taillandier
 * \version 1.0
 * \date February the 26th 2018
 *
 * This file defines the functions which evaluates the Lisp expressions, eval, eval_list and apply
 *
 */

#pragma once

#include <cassert>
#include "object.hpp"
#include "environment.hpp"
#include "subr.hpp"
#include "fsubr.hpp"


///\fn Object eval(const Object& obj, Environment& env)
/// Evaluates the expression according to the given environment
Object eval(const Object& obj, Environment& env);
/// \fn Object eval_list(const Object& obj, Environment& env)
/// Evaluate a list of expressions
Object eval_list(const Object& obj, Environment& env);
/// Applies a function to its arguments
Object apply(const Object& obj,
             const Object& lvals, Environment& env);
