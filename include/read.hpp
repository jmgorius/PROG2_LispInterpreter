/**
 * \file read.hpp
 * \brief Manages the console inputs
 * \author Vidal Attias, Jean-Michel Gorius, Quentin Le Dilavrec & Valentin Taillandier
 * \version 1.0
 * \date February the 26th 2018
 *
 * This file manages the console inputs and parses the stream
 *
 */

#pragma once

#include "object.hpp"
/// \fn Object read_object()
/// Reads the input file and return a parsed object
Object read_object();

///\fn void read_object_restart(FILE* fh)
/// Reinitializes the parser
void read_object_restart(FILE* fh);
