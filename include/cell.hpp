#pragma once

#include <string>
#include <functional>

///\class Object object.hpp
///Represents a memory cell
class Object;

/*****************************/

///\fn Cell cell.hpp
class Cell {
public:
  enum class sort {NUMBER, STRING, SYMBOL, PAIR, SUBR};
  ///\fn virtual sort get_sort() const = 0
  ///Returns the kind of cell
  virtual sort get_sort() const = 0;
};

/*****************************/
///\fn Cell_Number cell.hpp
///A cell containing a number
class Cell_Number: public Cell {
private:
  int contents;
public:
	///\fn sort get_sort() const override
	///Returns the kind of cell
	sort get_sort() const override;
	///\fn explicit Cell_Number(int _contents)
	///Constructor of Cell_Number
	explicit Cell_Number(int _contents);
	///\fn int get_contents() const
	///Returns the Cell's number
	int get_contents() const;
};

/*****************************/
///\fn Cell_String cell.hpp
///A cell containing a string
class Cell_String: public Cell {
private:
  std::string contents;
public:
	///\fn sort get_sort() const override
	///Returns the kind of cell
	sort get_sort() const override;
	///\fn explicit Cell_String(std::string s)
	///Constructor of Cell_String
	explicit Cell_String(std::string s);
	///\fn int get_contents() const
	///Returns the Cell's string
	std::string get_contents() const;
};

/*****************************/

class Cell_Symbol: public Cell {
private:
  std::string contents;
public:
	///\fn sort get_sort() const override
	///Returns the kind of cell
	sort get_sort() const override;
	///\fn explicit Cell_Symbol(std::string s)
	///Constructor of Cell_Symbol
	explicit Cell_Symbol(std::string s);
	///\fn std::string get_contents() const
	///Returns the Cell's symbol
	std::string get_contents() const;
};

/*****************************/

class Cell_Pair: public Cell {
private:
  Cell *car;
  Cell *cdr;
public:
	///\fn sort get_sort() const override
	///Returns the kind of cell
	sort get_sort() const override;
	///\fn explicit Cell_Pair(Cell *_car, Cell *_cdr)
	///Constructor of Cell_Pair
	explicit Cell_Pair(Cell *_car, Cell *_cdr);
	///\fn Cell *get_car() const
	///Returns the Cell's car
	Cell *get_car() const;
	///\fn Cell *get_cdr() const
	///Returns the Cell's cdr
	Cell *get_cdr() const;
};

/*****************************/

class Cell_Subr: public Cell {
private:
    using fun_type = std::function<Object(Object)>;
private:
  fun_type fun;
public:
	///\fn sort get_sort() const override
	///Returns the kind of cell
	sort get_sort() const override;
	///\fn explicit Cell_Subr(fun_type f)
	///Constructor of Cell_Subr
	explicit Cell_Subr(fun_type f);
	///\fn fun_type get_fun() const
	///Returns the Cell's function
	fun_type get_fun() const;
};
