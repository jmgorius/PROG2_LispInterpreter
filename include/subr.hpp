/**
 * \file subr.hpp
 * \brief Definition of the subr functions
 * \author Vidal Attias, Jean-Michel Gorius, Quentin Le Dilavrec & Valentin Taillandier
 * \version 1.0
 * \date February the 26th 2018
 *
 * This file defines all the built-in subr's
 *
 */


#pragma once

#include "object.hpp"

/// \namespace subr
/// This namespace contains all the subr symbols

namespace subr {
/// \fn inline const Object& plus()
/// The addition subr '+'
inline const Object& plus() {
  static const Object o = Object::fromSymbol("+");
  return o;
}

/// \fn inline const Object& mult()
/// The multiplication subr '*'
inline const Object& mult() {
  static const Object o = Object::fromSymbol("*");
  return o;
}

/// \fn inline const Object& minus()
/// The substraction subr '-'
inline const Object& minus() {
  static const Object o = Object::fromSymbol("-");
  return o;
}

/// \fn inline const Object& car()
/// The car subr 'car' which returns the head of a list
inline const Object& car() {
  static const Object o = Object::fromSymbol("car");
  return o;
}

/// \fn inline const Object& cdr()
/// The cdr subr 'cdr' which returns the tail of a list
inline const Object& cdr() {
  static const Object o = Object::fromSymbol("cdr");
  return o;
}

/// \fn inline const Object& cons()
/// The cons subr 'cons' wich returns a list which head is the first argument and the tail the second one
inline const Object& cons() {
  static const Object o = Object::fromSymbol("cons");
  return o;
}

/// \fn inline const Object& eq()
/// The eq subr 'eq' which tests if the two arguments have the same physical adress
inline const Object& eq() {
  static const Object o = Object::fromSymbol("eq");
  return o;
}

/// \fn inline const Object& equal()
/// The equal subr '=' which tests if the two arguments have the same structure
inline const Object& equal() {
  static const Object o = Object::fromSymbol("=");
  return o;
}

/// \fn inline const Object& read()
/// The raead subr 'read' returns the read value
inline const Object& read() {
  static const Object o = Object::fromSymbol("read");
  return o;
}

/// \fn inline const Object& print()
/// The print subr 'print' prints the argument's value
inline const Object& print() {
  static const Object o = Object::fromSymbol("print");
  return o;
}

/// \fn inline const Object& newline()
/// The newline subr 'newline' prints a new line
inline const Object& newline() {
  static const Object o = Object::fromSymbol("newline");
  return o;
}

/// \fn inline const Object& numberp()
/// The numberp subr 'numberp' tests wether the argument is a number
inline const Object& numberp() {
  static const Object o = Object::fromSymbol("numberp");
  return o;
}

/// \fn inline const Object& null()
/// The null subr 'null' tests wether the argument is the empty list
inline const Object& null() {
  static const Object o = Object::fromSymbol("null");
  return o;
}

/// \fn inline const Object& symbolp()
/// The symbolp subr 'symbolp' tests wether the argument is a symbol
inline const Object& symbolp() {
  static const Object o = Object::fromSymbol("symbolp");
  return o;
}

/// \fn inline const Object& stringp()
/// The stringp subr 'stringp' tests wether the argument is a string
inline const Object& stringp() {
  static const Object o = Object::fromSymbol("stringp");
  return o;
}

/// \fn inline const Object& listp()
/// The listp subr 'listp' tests wether the argument is a list
inline const Object& listp() {
  static const Object o = Object::fromSymbol("listp");
  return o;
}

/// \fn inline const Object& minus()
/// The substraction subr '-'
inline const Object& debug() {
  static const Object o = Object::fromSymbol("debug");
  return o;
}

/// \fn inline const Object& concat()
/// The concat subr 'concat' which concatenates the two lists arguments
inline const Object& concat() {
  static const Object o = Object::fromSymbol("concat");
  return o;
}

/// \fn inline const Object& eval()
/// The eval subr 'eval' evaluates the argument
inline const Object& eval() {
  static const Object o = Object::fromSymbol("eval");
  return o;
}

/// \fn inline const Object& apply()
/// The substraction subr '-'
inline const Object& apply() {
  static const Object o = Object::fromSymbol("apply");
  return o;
}

/// \fn inline const Object& error()
/// The error subr 'error' throws an error
inline const Object& error() {
  static const Object o = Object::fromSymbol("error");
  return o;
}

/// \fn inline const Object& end()
/// The end subr 'end' ends Lisp and kills the interpreter
inline const Object& end() {
  static const Object o = Object::fromSymbol("end");
  return o;
}
}

/// namespace subr::priv
/// This namespace contains all the subr's functions


namespace subr {
namespace priv {
Object do_eval(const Object& o);
Object do_apply(const Object& o);
Object do_plus(const Object& o);
Object do_mult(const Object& o);
Object do_minus(const Object& o);
Object do_car(const Object& o);
Object do_cdr(const Object& o);
Object do_cons(const Object& o);
Object do_concat(const Object& o);
Object do_eq(const Object& o);
Object do_equal(const Object& o);
Object do_read(const Object& o);
Object do_print(const Object& o);
Object do_newline(const Object& o);
Object do_end(const Object& o);
Object do_null(const Object& o);
Object do_stringp(const Object& o);
Object do_numberp(const Object& o);
Object do_symbolp(const Object& o);
Object do_listp(const Object& o);
Object do_error(const Object& o);
Object do_debug(const Object& o);
}
}
