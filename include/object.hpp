/**
 * \file object.hpp
 * \brief Definition of the Object class
 * \author Vidal Attias, Jean-Michel Gorius, Quentin Le Dilavrec & Valentin Taillandier
 * \version 1.0
 * \date February the 26th 2018
 *
 * This file defines the object class and the operations related with this class
 *
 */

#pragma once

#include <iostream>
#include <string>
#include <functional>

/// \class Cell cell.hpp
class Cell;

/// \class Object object.hpp
/// An Objects is anything the interpreter may deal with
class Object {
public:
  ///\fn Object() = default
  ///Default constructor
  Object() = default;
  ///\fn Object(const Object&) = default
  ///Constructs an Object from another Object
  Object(const Object&) = default;
  ///\fn ~Object() = default
  ///Object destructor
  ~Object() = default;
  ///\fn Object& operator=(const Object&) = default
  ///The assignment operator for Objects
  Object& operator=(const Object&) = default;

  ///\fn static const Object nil
  ///The nil Object which is equivalent to False
  static const Object nil;
  ///\fn static const Object t
  ///The True Object
  static const Object t;

  ///\fn static Object fromNumber(int n)
  ///Returns an Object from an integer
  static Object fromNumber(int n);
  ///\fn static Object fromString(const std::string& str)
  ///Returns an Object from a string
  static Object fromString(const std::string& str);
  ///\fn static Object fromSymbol(const std::string& str)
  ///Returns an Object from a symbol
  static Object fromSymbol(const std::string& str);
  ///\fn static Object fromBool(bool b)
  ///Returns an object from a boolean
  static Object fromBool(bool b);
  ///\fn static Object fromFunction(std::function<Object(Object)> f)
  ///Returns an Object from a function
  static Object fromFunction(std::function<Object(Object)> f);

  ///\fn int toNumber() const
  ///Returns an integer from an Object
  int toNumber() const;
  ///\fn std::string toString() const
  ///Returns a string from an Object
  std::string toString() const;
  /// \fn std::function<Object(Object)> toFunction() const;
  ///Returns a function from an Object
  std::function<Object(Object)> toFunction() const;

  ///\fn bool isNumber() const
  ///Tests wether an object is a number
  bool isNumber() const;
  ///\fn bool isString() const
  ///Tests wether an object is a string
  bool isString() const;
  ///\fn bool isSymbol() const
  ///Tests wether an object is a symbol
  bool isSymbol() const;
  ///\fn bool isList() const
  ///Tests wether an object is a list
  bool isList() const;
  ///\fn bool isSubr() const
  ///Tests wether an object is a subr
  bool isSubr() const;
  ///\fn bool isNull() const
  ///Tests wether an object is a null
  bool isNull() const;

  ///\fn friend bool operator==(const Object&, const Object&)
  ///Tests the equality between two Objects
  friend bool operator==(const Object&, const Object&);
  ///\fn friend Object cons(const Object& a, const Object& l)
  ///Returns a list Object which head is the first argument and the tail the second one
  friend Object cons(const Object& a, const Object& l);
  ///\fn friend Object car(const Object& l)
  ///Returns an Object which is the head of a list Object
  friend Object car(const Object& l);
  ///\fn friend Object cdr(const Object& l)
  ///Returns an Object which is the tail of a list Object
  friend Object cdr(const Object& l);

private:
  Cell* contents;
};

///\fn bool operator==(const Object& a, const Object& b)
///Tests the equality between two Objects
bool operator==(const Object& a, const Object& b);

///\fn std::ostream& operator<<(std::ostream& s, const Object& l)
/// Useful for printing an object in the console
std::ostream& operator<<(std::ostream& s, const Object& l);

///\fn Object cons(const Object& a, const Object& l)
///Returns an Object which is the head of a list Object
Object cons(const Object& a, const Object& l);
///\fn Object car(const Object& l)
///Returns an Object which is the head of a list Object
Object car(const Object& l);
///\fn Object cdr(const Object& l)
///Returns an Object which is the tail of a list Object
Object cdr(const Object& l);

///\fn Object cadr(const Object& l)
///Returns an Object which is the tail of the tail of a list Object
Object cadr(const Object& l);
///\fn Object cddr(const Object& l)
///Returns an Object which is the tail of the tail of a list Object
Object cddr(const Object& l);
///\fn Object caddr(const Object& l)
///Returns an Object which is the head of the tail of the tail of a list Object
Object caddr(const Object& l);
///\fn Object cdddr(const Object& l)
///Returns an Object which is the tail of the tail of the tail of a list Object
Object cdddr(const Object& l);
///\fn Object cddddr(const Object& l)
///Returns an Object which is the tail of the tail of the tail of the tail of a list Object
Object cadddr(const Object& l);
