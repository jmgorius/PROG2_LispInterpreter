#include <iostream>
#include <map>
#include "environment.hpp"
#include "object.hpp"
#include "read.hpp"

#define PASS(F) (std::cout << "\t" << F << "\tPASS" << std::endl)

#define FAIL(F, E, G) (std::cout << "\t" << F << "\tFAIL" << std::endl \
                                 << "expected: " << E << std::endl     \
                                 << "given:    " << G << std::endl)
#define PASS(F) (std::cout << "\t" << F << "\tPASS" << std::endl)

#define COUT_TEST(T, F, E, G) (T \
        ? PASS(F)                \
        : FAIL(F, E, G))

#define ADD(S, V)                             \
    a.add_binding(Object::fromSymbol(#S), V); \
    std::cout << "\t" << #S << " <- " << #V << ";"

#define COMP(S, E, MORE)                            \
    Object r = a.get_value(Object::fromSymbol(#S)); \
    COUT_TEST(r == E, MORE #S "==" #E, E, r)

template <class T>
inline auto test(T a, T b) {
  if (a == b) {
    PASS(a << "==");
  } else {
    FAIL(a << "==" << b, a, b);
  }
}

/** Automaticaly expand tests
 * ^\+(.*) (.*)
 * a.add_binding(Object::fromSymbol("a"), Object::nil);
 * ex: + a rzgbzrgbzr(-'_('_))
 * becomes a.add_binding(Object::fromSymbol(" a"), rzgbzrgbzr(-'_('_)));
 *
 * ^\=(.*) (.*)
 * auto r = a.get_value(Object::fromSymbol("$1"));
 * ex: + a rzgbzrgbzr(-'_('_))
 * becomes a.add_binding(Object::fromSymbol(" a"), rzgbzrgbzr(-'_('_)));
 *
 **/

int main(int argc, char** argv) {

  try {
    unsigned batch_failed = 0;
    std::cout << "+++++start+++++++TEST++++"
              << "add_binding/get_value"
              << "++++++" << std::endl;
    {
      Environment a;
      a.add_binding(Object::fromSymbol("a"), Object::nil);
      auto r = a.get_value(Object::fromSymbol("a"));
      COUT_TEST(
        Object::nil == r,
        "add_binding(<a>,nil);get_value(<a>)==nil;",
        Object::nil, r);
    }
    {
      Environment a;
      a.add_binding(Object::fromSymbol("a"), Object::nil);
      a.add_binding(Object::fromSymbol("a"), Object::nil);
      auto r = a.get_value(Object::fromSymbol("a"));
      COUT_TEST(
        Object::nil == r,
        "<a> <- nil;<a> <- nil;<a> == nil",
        Object::nil, r);
    }
    {
      Environment a;
      a.add_binding(Object::fromSymbol("a"), Object::t);
      auto r = a.get_value(Object::fromSymbol("a"));
      COUT_TEST(
        Object::t == r,
        "<a> <- t;<a> == t",
        Object::t, r);
    }
    {
      Environment a;
      a.add_binding(Object::fromSymbol("a"), Object::nil);
      a.add_binding(Object::fromSymbol("a"), Object::t);
      auto r = a.get_value(Object::fromSymbol("a"));
      COUT_TEST(
        Object::t == r,
        "<a> <- nil;<a> <- t;<a> == t",
        Object::t, r);
    }
    {
      Environment a;
      a.add_binding(Object::fromSymbol("a"), Object::t);
      a.add_binding(Object::fromSymbol("a"), Object::nil);
      a.add_binding(Object::fromSymbol("a"), Object::t);
      auto t = Object::t;
      COMP(a, t, "<a> <- t;<a> <- nil;<a> <- t;");
    }
    {
      Environment a;
      a.add_binding(Object::fromSymbol("a"), Object::t);
      a.add_binding(Object::fromSymbol("a"), Object::nil);
      COMP(a, Object::nil, "a <- nil;");
    }
    {
      auto t = Object::t;
      auto nil = Object::nil;
      Environment a;
      ADD(a, t);
      ADD(a, nil);
      COMP(a, nil, "");
    }
    std::cout << "+++++end+++++++++TEST+++" << "environment" << "++++++"
              << batch_failed << " failed" << std::endl;

  } catch (...) {
    std::cout << "some tests thrown exceptions" << std::endl;
    return 1;
  }

  return 0;
}
