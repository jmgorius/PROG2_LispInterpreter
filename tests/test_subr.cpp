#include <functional>
#include <iostream>
#include <limits>
#include "environment.hpp"
#include "object.hpp"
#include "subr.hpp"

#define MAX_TESTS (100)
#define INT_MAX (int_max)
static const int int_max = std::numeric_limits<int>::max();

// static const int max_tests=100;
using namespace subr;

void num_tests(unsigned& batch_failed,
               std::function<Object(Object)> f_l,
               std::function<int(int, int)> f_c,
               std::string f_l_symb,
               std::string f_c_symb);
void ccc_tests(unsigned& batch_failed);

Object cons_aux(unsigned depth, int& count);

int car_cdr_aux(const Object& o);

int main(int argc, char** argv) {
  /* initialize random seed: */
  srand(time(NULL));

  //catch every throw
  try {
    unsigned batch_failed = 0;
    // ccc_tests(batch_failed);
    // batch_failed = 0;
    num_tests(batch_failed,
              subr::priv::do_plus,
              std::plus<int>(), "do_plus", "+");
    batch_failed = 0;
    num_tests(batch_failed,
              subr::priv::do_minus,
              std::minus<int>(), "do_minus", "-");
    batch_failed = 0;
    num_tests(batch_failed,
              subr::priv::do_mult,
              std::multiplies<int>(), "do_mult", "*");
    batch_failed = 0;

  } catch (...) {
    std::cout << "some tests thrown exceptions" << std::endl;
    return 1;
  }

  return 0;
}

void num_tests(unsigned& batch_failed,
               std::function<Object(Object)> f_l,
               std::function<int(int, int)> f_c,
               std::string f_l_symb,
               std::string f_c_symb) {
  std::cout << "+++++start+++++++TEST++++" << f_l_symb << "++++++" << std::endl;
  for (int i = 0; i < MAX_TESTS; i++) {
    auto a = rand() % INT_MAX,
         b = rand() % INT_MAX;
    auto p = cons(
               Object::fromNumber(a),
               cons(Object::fromNumber(b), Object::nil));
    auto r_l = f_l(p);
    auto r_c = f_c(a, b);
    bool t = r_c == r_l.toNumber();
    if (t) {
      std::cout << "\t" << a << f_c_symb << b << "\tPASS";
    } else {
      std::cout << "\t" << a << f_c_symb << b << "\tFAIL" << std::endl
                << "expected: " << r_c << std::endl
                << "given:    " << r_l;
      ++batch_failed;
    }

    std::cout << std::endl;
  }
  std::cout << "+++++end+++++++++TEST+++" << f_l_symb << "++++++"
            << MAX_TESTS - batch_failed << "/" << MAX_TESTS << std::endl;
}

void ccc_tests(unsigned& batch_failed) {
  std::cout << "+++++start+++++++TEST++++"
            << "cons car cdr"
            << "++++++" << std::endl;
  for (int i = 0; i < MAX_TESTS; i++) {
    auto a = rand() % 1000;
    int count = 0;
    auto p0 = cons_aux(a, ++count);
    auto p = car_cdr_aux(p0);
    bool t = count == p;
    if (t) {
      std::cout << "\t" << a << " cons car cdr " << p << " " << p0 << "\tPASS";
    } else {
      std::cout << "\t" << a << " cons car cdr " << p << " " << p0 << "\tFAIL" << std::endl
                << "expected: " << count << std::endl
                << "given:    " << p;
      ++batch_failed;
    }

    std::cout << std::endl;
  }
  std::cout << "+++++end+++++++++TEST+++"
            << "cons car cdr"
            << "++++++"
            << MAX_TESTS - batch_failed << "/" << MAX_TESTS << std::endl;
}

Object cons_aux(unsigned depth, int& count) {
  if (depth <= 0)
    return Object::nil;

  const auto& a = (rand() % 2048 > 1024)
                  ? cons_aux(--depth, ++count)
                  : Object::nil;
  const auto& b = (rand() % 2048 > 1024)
                  ? cons_aux(--depth, ++count)
                  : Object::nil;
  // std::cout << "aaa" << std::endl;
  return subr::priv::do_cons(cons(Object::fromSymbol("cons"), cons(a, b)));
}

int car_cdr_aux(const Object& o) {
  if (o.isNull()) {
    std::cout << "aaa " << o << std::endl;

    return 0;
  }
  if (o.isList()) {
    // std::cout << "bbb " << o << std::endl;

    const auto& a = car_cdr_aux(subr::priv::do_car(o));
    const auto& b = car_cdr_aux(subr::priv::do_cdr(o));

    return a + b + 1;
  }

  return 0;
}
