
# LISP interpreter in C++   

###### Project group: Attias_Gorius_Le-Dilavrec_Taillandier

 *The aim of this project is to build a dynamic LISP intepreter in C++.
 You can use the Makefile build and run the interpreter:*

________________________________________________________________________________

> To build the the binary:
>
>     make lisp


> To clean the buildings files and the binary:
>     
>     make clean


>  To run the interpreter:
>     
>     make run


> To test the interpreter with a unit test serie:
>
>     make test


>   To launch a quick demo:
>
>     make demo


>   To build the documentation:
>
>     make doc