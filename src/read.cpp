#include <iostream>
#include <stdexcept>

#include "object.hpp"
#include "read.hpp"

using namespace std;

extern Object get_read_Object();
extern int yyparse();
extern void yyrestart(FILE *fh);

Object read_object() {
  if (yyparse() != 0)
    throw runtime_error("End of input stream");
  Object l = get_read_Object();
  //clog << "Read: " << l << endl;
  return l;
}

void read_object_restart(FILE* fh) {
  yyrestart(fh);
}
