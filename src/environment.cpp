#include "environment.hpp"

Environment global_env;

void Environment::add_binding(const Object& symbol, const Object& value) {
  bindings.push_back(binding(symbol.toString(), value));
}


const Object& Environment::get_value (const Object& key) const {
  for(size_t i = bindings.size() - 1; i > 0; --i) {
    if(bindings[i].first == key.toString())
      return bindings[i].second;
  }
  throw UnknownSymbolError(key);
}

void Environment::add_largs(const Object& lpars, const Object& lvals) {
  if(lpars.isNull()) {
    if(!lvals.isNull())
      throw LispError("Too many arguments", lvals);
  } else {
    if(lvals.isNull())
      LispError("Too many parameters", lpars);
    add_binding(car(lpars), car(lvals));
    add_largs(cdr(lpars), cdr(lvals));
  }
}

void Environment::print() const {
  print_level(max_print_level);
}

void Environment::print_level(int n) const {
  if(n <= 0)
    std::cout << "<env>";
  else if(!bindings.empty()) {
    for(const binding& b : bindings) {
      std::cout << "\"" << b.first << "\"" << " = ";
      std::cout << b.second;
      std::cout << "; ";
    }
  }
}
