#include "eval.hpp"

Object eval(const Object &obj, Environment& env) {
  if(obj.isNull())
    return obj;
  else if(obj == Object::t)
    return obj;
  else if(obj.isNumber())
    return obj;
  else if(obj.isString())
    return obj;
  else if(obj.isSubr())
    return obj;
  else if(obj.isSymbol())
    return env.get_value(obj);
  else { // une llist
    if(car(obj) == fsubr::lambda())
      return obj;
    else if(car(obj) == fsubr::let())
      return fsubr::priv::do_let(obj, env);
    else if(car(obj) == fsubr::printenv()) {
      env.print();
      return Object::nil;
    } else if(car(obj) == fsubr::quote())
      return cadr(obj);
    else if(car(obj) == fsubr::_if())
      return fsubr::priv::do_if(cdr(obj), env);
    else if(car(obj) == fsubr::cond())
      return fsubr::priv::do_cond(cdr(obj), env);
    else if(car(obj) == fsubr::progn())
      return fsubr::priv::do_progn(cdr(obj), env);
    else {
      Object f = car(obj);
      Object largs = cdr(obj);
      Object lvals = eval_list(largs, env);
      return apply(f, lvals, env);
    }
  }
}

Object eval_list(const Object &obj, Environment &env) {
  if(obj.isNull()) {
    return Object::nil;
  } else {
    return cons(eval(car(obj), env), eval_list(cdr(obj), env));
  }

}

//for lambda function
Object apply(const Object &obj, const Object &lvals, Environment& env) {
  if(obj.isNull() || obj.isNumber() || obj.isString()) {
    throw ApplyError(obj);
  } else if(obj.isSubr()) {
    return obj.toFunction()(lvals);
  } else if(obj.isSymbol()) {
    return apply(eval(obj, env), lvals, env);
  } else if(obj.isList() && car(obj) == fsubr::lambda()) {
    return fsubr::priv::do_lambda(obj, lvals, env);

  } else {
    throw ApplyError(obj);
  }


}
