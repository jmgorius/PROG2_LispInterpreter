#include "object.hpp"

#include <iostream>
#include <cassert>

#include "cell.hpp"

namespace {
static void print_list(std::ostream& s, const Object& l) {
  assert(l.isList());
  if(l.isNull())
    return;
  if(cdr(l).isNull()) {
    s << car(l);
    return;
  }
  s << car(l) << " ";
  print_list(s, cdr(l));
}
}

const Object Object::nil = Object::fromSymbol("nil");
const Object Object::t = Object::fromSymbol("t");

Object Object::fromNumber(int n) {
  Object res;
  res.contents = new Cell_Number(n);
  return res;
}

Object Object::fromString(const std::string& s) {
  Object res;
  res.contents = new Cell_String(s);
  return res;
}

Object Object::fromSymbol(const std::string& s) {
  Object res;
  res.contents = new Cell_Symbol(s);
  return res;
}

Object Object::fromBool(bool b) {
  return b ? Object::t : Object::nil;
}

Object Object::fromFunction(std::function<Object(Object)> f) {
  Object res;
  res.contents = new Cell_Subr(f);
  return res;
}

int Object::toNumber() const {
  assert(isNumber());
  return (dynamic_cast<Cell_Number*>(contents))->get_contents();
}

std::string Object::toString() const {
  assert(isString() || isSymbol());
  if(isString())
    return (dynamic_cast<Cell_String*>(contents))->get_contents();
  return (dynamic_cast<Cell_Symbol*>(contents))->get_contents();
}

std::function<Object(Object)> Object::toFunction() const {
  assert(isSubr());
  return (dynamic_cast<Cell_Subr*>(contents))->get_fun();
}

bool Object::isNumber() const {
  assert(contents != nullptr);
  return (contents->get_sort() == Cell::sort::NUMBER);
}

bool Object::isString() const {
  assert(contents != nullptr);
  return (contents->get_sort() == Cell::sort::STRING);
}

bool Object::isSymbol() const {
  assert(contents != nullptr);
  return (contents->get_sort() == Cell::sort::SYMBOL);
}

bool Object::isList() const {
  assert(contents != nullptr);
  return (contents->get_sort() == Cell::sort::PAIR);
}

bool Object::isSubr() const {
  assert(contents != nullptr);
  return (contents->get_sort() == Cell::sort::SUBR);
}

bool Object::isNull() const {
  return *this == Object::nil;
}

bool operator==(const Object& a, const Object& b) {
  assert(a.contents != nullptr);
  assert(b.contents != nullptr);
  if (a.contents->get_sort() != b.contents->get_sort()) return false;
  if (a.isNumber()) return a.toNumber() == b.toNumber();
  if (a.isString() || a.isSymbol()) return a.toString() == b.toString();
  return a.contents == b.contents;
}

std::ostream& operator<<(std::ostream& s, const Object& obj) {
  if(obj.isNull())
    return s << "()";
  if(obj.isNumber())
    return s << obj.toNumber();
  if(obj.isString() || obj.isSymbol())
    return s << obj.toString();
  if(obj.isSubr())
    return s << "<subr>";

  assert(obj.isList());
  s << "(";
  print_list(s, obj);
  s << ")";
  return s;
}

Object cons(const Object& a, const Object& l) {
  Object res;
  res.contents = new Cell_Pair(a.contents, l.contents);
  return res;
}

Object car(const Object& l) {
  assert(l.isList());
  Object res;
  res.contents = (dynamic_cast<Cell_Pair*>(l.contents))->get_car();
  return res;
}

Object cdr(const Object& l) {
  assert(l.isList());
  Object res;
  res.contents = (dynamic_cast<Cell_Pair*>(l.contents))->get_cdr();
  return res;
}

Object cadr(const Object& l) {
  return car(cdr(l));
}

Object cddr(const Object& l) {
  return cdr(cdr(l));
}

Object caddr(const Object& l) {
  return car(cddr(l));
}

Object cdddr(const Object& l) {
  return cdr(cddr(l));
}

Object cadddr(const Object& l) {
  return car(cdddr(l));
}
