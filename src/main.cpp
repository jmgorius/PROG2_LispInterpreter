#include <iostream>
#include "object.hpp"
#include "read.hpp"
#include "toplevel.hpp"
#include "environment.hpp"
#include "eval.hpp"


int main() {
  init();
  while(true) {
    std::clog << "Lisp > " << std::flush;
    Object l = read_object();
    if(!handle_directive(l))
      std::cout << eval(l, global_env) << std::endl;
  }
}
