#include <cassert>
#include "subr.hpp"
#include "eval.hpp"
#include "iostream"
#include "environment.hpp"

namespace subr::priv {
Object do_eval(const Object& o) {
  return eval(cadr(o), global_env);
}
Object do_apply(const Object& o) {
  return o;
}
Object do_plus(const Object& o) {
  return Object::fromNumber(car(o).toNumber() + cadr(o).toNumber());
}
Object do_mult(const Object& o) {
  return Object::fromNumber(car(o).toNumber() * cadr(o).toNumber());
}
Object do_minus(const Object& o) {
  return Object::fromNumber(car(o).toNumber() - cadr(o).toNumber());
}
Object do_car(const Object& o) {
  assert(o.isList());
  return car(o);
}
Object do_cdr(const Object& o) {
  assert(o.isList());
  return cdr(o);
}
Object do_cons(const Object& o) {
  return cons(car(o), cadr(o));
}
Object do_concat(const Object& o) {
  assert(o.isList());
  if(o.isNull())
    return Object::fromString("");
  std::string s = car(o).toString();
  std::string t = do_concat(cdr(o)).toString();
  return Object::fromString(s + t);
}
Object do_eq(const Object& o) {
  return Object::fromBool(car(o) == cadr(o));
}
Object do_equal(const Object& o) {
  return Object::fromBool(car(o) == cadr(o));
}
Object do_read(const Object& o) {
  //TODO
  return o;
}
Object do_print(const Object& o) {
  std::cout << o << std::endl;
  return Object::nil;
}
Object do_newline(const Object& o) {
  std::cout << std::endl << std::flush;
  return Object::nil;
}
Object do_end(const Object& o) {
  //TODO
  return o;
}
Object do_null(const Object& o) {
  //TODO
  return o;
}
Object do_stringp(const Object& o) {
  return Object::fromBool(o.isString());
}
Object do_numberp(const Object& o) {
  return Object::fromBool(o.isNumber());
}
Object do_symbolp(const Object& o) {
  return Object::fromBool(o.isSymbol());
}
Object do_listp(const Object& o) {
  return Object::fromBool(o.isList());
}
Object do_error(const Object& o) {
  //TODO
  return o;
}
Object do_debug(const Object& o) {
  //TODO
  return o;
}
}
