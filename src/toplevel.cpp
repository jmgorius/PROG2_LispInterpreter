#include "object.hpp"
#include "subr.hpp"
#include "environment.hpp"
#include "eval.hpp"
#include "object.hpp"

#include <cassert>

namespace {
static void handle_set(const Object& obj) {
  /*Object name = cadr(obj);
  Object value = caddr(obj);

  global_env.add_binding(name, eval(value, global_env));
  std::cout << "SET: " << name << " = " << value << std::endl;*/

  Object defined_obj = cadr(obj);
  Object defining_expr;

  if(cdddr(obj).isNull())
    defining_expr = caddr(obj);
  else {
    Object rest = cddr(obj);
    defining_expr = cons(fsubr::lambda(), rest);
  }

  std::string defined_name = defined_obj.toString();
  Object defining_value = eval(defining_expr, global_env);
  std::cout << "SET: " << defined_name << " = " << defining_value
            << std::endl;
  global_env.add_binding(defined_obj, defining_value);

}

static void handle_load(const Object&) {

}
}

void init() {
  global_env.add_binding(subr::plus(),Object::fromFunction(subr::priv::do_plus));
  global_env.add_binding(subr::mult(),Object::fromFunction(subr::priv::do_mult));
  global_env.add_binding(subr::minus(),Object::fromFunction(subr::priv::do_minus));
  global_env.add_binding(subr::car(),Object::fromFunction(subr::priv::do_car));
  global_env.add_binding(subr::cdr(),Object::fromFunction(subr::priv::do_cdr));
  global_env.add_binding(subr::cons(),Object::fromFunction(subr::priv::do_cons));
  global_env.add_binding(subr::eq(),Object::fromFunction(subr::priv::do_equal));
  global_env.add_binding(subr::read(),Object::fromFunction(subr::priv::do_read));
  global_env.add_binding(subr::print(),Object::fromFunction(subr::priv::do_print));
  global_env.add_binding(subr::newline(),Object::fromFunction(subr::priv::do_newline));
  global_env.add_binding(subr::numberp(),Object::fromFunction(subr::priv::do_numberp));
  global_env.add_binding(subr::null(),Object::fromFunction(subr::priv::do_null));
  global_env.add_binding(subr::symbolp(),Object::fromFunction(subr::priv::do_symbolp));
  global_env.add_binding(subr::stringp(),Object::fromFunction(subr::priv::do_stringp));
  global_env.add_binding(subr::listp(),Object::fromFunction(subr::priv::do_listp));
  global_env.add_binding(subr::debug(),Object::fromFunction(subr::priv::do_debug));
  global_env.add_binding(subr::concat(),Object::fromFunction(subr::priv::do_concat));
  global_env.add_binding(subr::eval(),Object::fromFunction(subr::priv::do_eval));
  global_env.add_binding(subr::apply(),Object::fromFunction(subr::priv::do_apply));
  global_env.add_binding(subr::error(),Object::fromFunction(subr::priv::do_error));
  global_env.add_binding(subr::end(),Object::fromFunction(subr::priv::do_end));
  global_env.add_binding(subr::equal(),Object::fromFunction(subr::priv::do_equal));
}
void restart() {
  init();
}
bool handle_directive(const Object& obj) {
  if(!obj.isList())
    return false;

  Object directive = car(obj);

  if(directive == fsubr::load()) {
    handle_load(obj);
    return true;
  } else if((directive == fsubr::setq()) || (directive ==  fsubr::defun())) {
    handle_set(obj);
    return true;
  }

  return false;
}
