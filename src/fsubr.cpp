#include <cassert>
#include "fsubr.hpp"
#include "eval.hpp"
#include "iostream"


namespace {

Object map_car(const Object& obj) {
  assert(obj.isList());
  if(obj.isNull())
    return obj;
  else
    return cons(car(car(obj)), map_car(cdr(obj)));
}

Object map_cadr(const Object& obj) {
  assert(obj.isList());
  if(obj.isList())
    return obj;
  else
    return cons(cadr(car(obj)), map_cadr(cdr(obj)));
}

}

namespace fsubr {
namespace priv {

Object do_lambda(const Object& obj, const Object &lvals, Environment& env) {
  Object body = caddr(obj);
  Object lpars = cadr(obj);
  size_t prev_size = env.size();
  env.add_largs(lpars, lvals);
  Object res = eval(body, env);
  std::clog << lvals << std::endl;
  env.print();
  size_t new_size = env.size();
  for(; new_size - prev_size > 0; --new_size)
    env.pop_binding();
  return res;
}
Object do_quote(const Object& obj, const Environment& env) {
  return obj;
}
Object do_progn(const Object& obj,  Environment& env) {
  assert(obj.isList());
  if(obj.isNull())
    return Object::nil;
  else if((cdr(obj)).isNull())
    return eval(car(obj), env);
  else {
    eval(car(obj), env);
    return do_progn(cdr(obj), env);
  }

}
Object do_printenv(const Object& obj, const Environment& env) {
  env.print();
  return Object::nil;
}

Object do_let(const Object& obj,  Environment& env) {
  Object binding_list = cadr(obj);
  Object body = caddr(obj);
  Object lpars = map_car(binding_list);
  Object largs = map_cadr(binding_list);
  return eval(cons(fsubr::lambda(),
                   cons(lpars, cons(body, cons(largs, Object::nil)))), env);
}

Object do_if(const Object& obj,  Environment& env) {
  assert(obj.isList());
  Object test_part = car(obj);
  Object then_part = cadr(obj);
  Object else_part = caddr(obj);
  if(!eval(test_part, env).isNull())
    return eval(then_part, env);
  else
    return eval(else_part, env);
}

Object do_cond(const Object& obj,  Environment& env) {
  assert(obj.isList());
  if(obj.isNull())
    return Object::nil;
  else {
    Object test_part = cadr(obj);
    Object then_part = caddr(obj);
    if(!((eval(test_part, env)).isNull()))
      return eval(then_part, env);
    else
      return do_cond(cdr(obj), env);
  }
}

}
}
